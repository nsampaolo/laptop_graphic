//FileName:		Viewcontroller.cpp
//Programmer:	Dan Cliburn// Chris Bolt, Nicos Sampaolo, Carter Hafif
//Date:			9/9/2014
//Purpose:		This file defines the methods for the Viewcontroller class
//See:  http://www.sdltutorials.com/sdl-tutorial-basics/
//		http://www.sdltutorials.com/sdl-opengl-tutorial-basics/
//		http://stackoverflow.com/questions/13826150/sdl2-opengl3-how-to-initialize-sdl-inside-a-function
//for many more details on how to write an OpenGL program using SDL.  You might also want to go to these 
//pages which will link you to other tutorials on how to do stuff with SDL.
//Be warned, however, that a lot of the tutorials describe SDL 1.2, but we will be using SDL 2 in this course.

#include <SDL.h>
#include <glew.h>  //glew.h is supposed to be included before gl.h.  To be safe, you can just include glew.h instead
#include "viewcontroller.h"
#include "LoadShaders.h"
#include "vmath.h"
#include <iostream>
#include <time.h>
using namespace std;

const int WINDOWWIDTH = 800;
const int WINDOWHEIGHT = 800;

Viewcontroller::Viewcontroller()
{
	quit = false;
	window = 0;
	ogl4context = 0;

	PVM_matrixLoc = 0;
	model_matrix = vmath::mat4::identity();
	view_matrix = vmath::mat4::identity();
	projection_matrix = vmath::mat4::identity();

	ROTATING = false;
	xAngle = yAngle = 0.0;
	lastOffsetX = lastOffsetY = 0;
}

//Initializes SDL, GLEW, and OpenGL
bool Viewcontroller::init()
{
	isOpen = true;
	//First initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		cout << "Failed to initialize SDL." << endl;
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, true);
	if ((window = SDL_CreateWindow("Lab 7 Example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOWWIDTH, WINDOWHEIGHT, SDL_WINDOW_OPENGL)) == NULL)
	{
		cout << "Failed to create window." << endl;
		return false;
	}
	ogl4context = SDL_GL_CreateContext(window);

	//Next initialize GLEW
    GLenum err = glewInit();
    if( GLEW_OK != err )
    {
        cout <<"Error initializing GLEW: " << glewGetErrorString(err) << endl;
		return false;
    }

	glClearColor(0.5, 0.5, 0.5, 1.0);
	glEnable(GL_DEPTH_TEST);
	
	//The following code was adapted from the OpenGL 4.0 Shading Language Cookbook, by David Wolff
	//to provide information about the hardware and supported versions of OpenGL and GLSL. 
	const GLubyte *renderer = glGetString( GL_RENDERER );
    const GLubyte *vendor = glGetString( GL_VENDOR );
    const GLubyte *version = glGetString( GL_VERSION );
    const GLubyte *glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
	cout << "GL Vendor: " <<  vendor << endl;
    cout << "GL Renderer: " <<  renderer << endl;
    cout << "GL Version: " << version << endl;
	cout << "GLSL Version: " << glslVersion << endl << endl;

    //Finally, we need to load the shaders. I am using the LoadShaders() function written  
	//by the authors of the OpenGL Programming Guide (8th edition).
	ShaderInfo shaders[] = {
		{ GL_VERTEX_SHADER, "shader.vert"},
		{ GL_FRAGMENT_SHADER, "shader.frag"},
		{ GL_NONE, NULL}
	};
	GLuint program;
	if ( (program = LoadShaders(shaders)) == 0)
	{
		cout << "Error Loading Shaders" << endl;
		return false;
	}
	glUseProgram(program);

	//Now initialize all of our models
	GLfloat green[4] = { 0.0, 0.0, 0.0, 1.0 };
	tiger.init("images/sad_kim.bmp", green);
	pacific.init("images/kb.bmp", green);
	back.init("images/back.bmp", green);
	bottom.init("images/bottom.bmp", green);
	sides.init(green);

	//Find the location of the PVM_matrix in the shader
	PVM_matrixLoc = glGetUniformLocation(program, "PVM_matrix");
	numTexLoc = glGetUniformLocation(program, "numTextures");

	//Since the projection and view orientation matrices will not change during the program we will calculate them here
	projection_matrix = vmath::frustum(-0.1, 0.1, -0.1, 0.1, 0.1, 20.0);
	vmath::vec3 eye = vmath::vec3(1.5, 1.5, 1.5);
	vmath::vec3 aim = vmath::vec3(0.0, 0.0, 0.0);
	vmath::vec3 up = vmath::vec3(0.0, 1.0, 0.0);
	view_matrix = vmath::lookat(eye, aim, up);

	return true;  //Everything got initialized
}

//Display what we want to see in the graphics window
void Viewcontroller::display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUniform1i(numTexLoc, 1);
	vmath::mat4 baseModelMatrixTop;
	//Calculate the rotation based on the xAngle and yAngle variables computed in handleEvents
	vmath::mat4 baseModelMatrix = vmath::rotate(yAngle, 0.0f, 1.0f, 0.0f) * vmath::rotate(xAngle, 1.0f, 0.0f, 0.0f);
	if (isOpen == true)
		baseModelMatrixTop = vmath::rotate(yAngle, 0.0f, 1.0f, 0.0f) * vmath::rotate(xAngle, 1.0f, 0.0f, 0.0f);//keeps the screen starting from the origin
	else 
		baseModelMatrixTop = vmath::rotate(yAngle, 0.0f, 1.0f, 0.0f) * vmath::rotate(xAngle, 1.0f, 0.0f, 0.0f)* vmath::translate(0.0f, -1.0f, 1.0f) * vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);// shifts the screen to start at the closed position
	vmath::mat4 scale_matrix = vmath::mat4::identity();

	//Remember that the matrices are applied to vertices in the opposite order
	//in which they are specified below (i.e. model_matrix is applied first)
	model_matrix = baseModelMatrixTop;


	vmath::mat4 PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);
	
	tiger.draw();

	model_matrix = baseModelMatrixTop * vmath::translate(0.0f, 0.0f, -0.1f);
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	back.draw();
	

	model_matrix = baseModelMatrix * vmath::translate(0.0f, -1.0f, 1.0f) * vmath::rotate(-90.0f, 1.0f, 0.0f, 0.0f);
	
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);
	
	pacific.draw();

	model_matrix = baseModelMatrix * vmath::translate(0.0f, -1.1f, 1.0f) * vmath::rotate(-90.0f, 1.0f, 0.0f, 0.0f);
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	bottom.draw();

	glUniform1i(numTexLoc, 0);

	
	model_matrix = baseModelMatrix * vmath::translate(1.0f, -1.05f, 1.0f) * vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f) * vmath::scale(1.0f,0.05f,1.0f);//bottom right side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrix * vmath::translate(-1.0f, -1.05f, 1.0f) * vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f) * vmath::scale(1.0f, 0.05f, 1.0f);//bottom left side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrix * vmath::translate(0.0f, -1.05f, 0.0f) * vmath::scale(1.0f, 0.05f, 1.0f);//bottom back side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrix * vmath::translate(0.0f, -1.05f, 2.0f) * vmath::scale(1.0f, 0.05f, 1.0f);//bottom front side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();



	model_matrix = baseModelMatrixTop * vmath::translate(1.0f, 0.0f, -0.05f) * vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f) * vmath::scale(0.05f, 1.0f, 1.0f);//top right side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrixTop * vmath::translate(-1.0f, 0.0f, -0.05f) * vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f) * vmath::scale(0.05f, 1.0f, 1.0f);//top left side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrixTop * vmath::translate(0.0f, -1.0f, -0.05f) * vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f) * vmath::scale(1.0f, 0.05f, 1.0f);//top back side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	model_matrix = baseModelMatrixTop * vmath::translate(0.0f, 1.0f, -0.05f) * vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f) * vmath::scale(1.0f, 0.05f, 1.0f);//top front side
	PVMmatrix = projection_matrix * view_matrix * model_matrix;
	glUniformMatrix4fv(PVM_matrixLoc, 1, GL_FALSE, PVMmatrix);

	sides.draw();

	glFlush();
	SDL_GL_SwapWindow(window);
}


bool Viewcontroller::handleEvents(SDL_Event *theEvent)
{

	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_UP]) {
		if (isOpen == true)
		{
			isOpen = false;
		}
		else
		{
			isOpen = true;
		}
	}


	switch(theEvent->type)
	{
		case SDL_QUIT:  //user clicked on the 'X' in the window
		{
			return true;  //force program to quit
		}
		case SDL_MOUSEBUTTONDOWN:
		{
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(1))  //Attach rotation to the left mouse button
			{
				// save position where button down event occurred. This 
				// is the "zero" position for subsequent mouseMotion callbacks. 
				baseX = theEvent->button.x;
				baseY = theEvent->button.y;
				ROTATING = true;
			}
			break;
		}
		case SDL_MOUSEBUTTONUP:
		{
			if (ROTATING)  //are we finishing a rotation?
			{
				//Remember where the motion ended, so we can pick up from here next time. 
				lastOffsetX += (theEvent->button.x - baseX);
				lastOffsetY += (theEvent->button.y - baseY);
				ROTATING = false;
			}
			break;
		}
		case SDL_MOUSEMOTION:
		{
			//Is the left mouse button also down?
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(1))
			{
				float x, y;

				//Calculating the conversion => window size to angle in degrees 
				float scaleX = 360.0 / WINDOWWIDTH;
				float scaleY = 360.0 / WINDOWHEIGHT;

				x = (theEvent->button.x - baseX) + lastOffsetX;
				y = (theEvent->button.y - baseY) + lastOffsetY;

				// map "x" to a rotation about the y-axis. 
				x *= scaleX;
				yAngle = x;

				// map "y" to a rotation about the x-axis. 
				y *= scaleY;
				xAngle = y;
			}
			break;
		}
	/*	case SDL:
		{
			if (isOpen == true)
			{
				isOpen = false;
			}
			else
			{
				isOpen = true;
			}
			break;
		}*/
	} //end the switch
	return false;  //the program should not end
}

void Viewcontroller::run()
{
	if (init() == false)  //This method (defined above) sets up OpenGL, SDL, and GLEW
	{
		cout << "Program failed to initialize ... exiting." << endl;
		return;
	}

	SDL_Event events;  //Makes an SDL_Events object that we can use to handle events

	//The following is the main program loop.  Essentially it keeps running until the
	//user clicks on the 'X' in the graphics window.
	do
	{
		display();  //This method (defined above) draws whatever we have defined
		while (SDL_PollEvent(&events)) //keep processing the events as long as there are events to process
		{
			quit = handleEvents(&events);
		}

	} while (!quit); //run until "quit" is true

	SDL_GL_DeleteContext(ogl4context);
	SDL_DestroyWindow(window);
	SDL_Quit();
}