//FileName:		viewcontroller.h
//Programmer:	Dan Cliburn// Chris Bolt, Nicos Sampaolo, Carter Hafif
//Date:			9/9/2014
//Purpose:		This file defines the header for the Viewcontroller class

#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H

#include <SDL.h>
#include "texmodel.h"
#include "vmath.h"

class Viewcontroller
{
	private:
		bool quit;

		GLint PVM_matrixLoc;
		vmath::mat4 model_matrix;
		vmath::mat4 view_matrix;
		vmath::mat4 projection_matrix;

		TexModel tiger;
		TexModel pacific;
		TexModel back;
		TexModel bottom;
		Quad sides;
		GLint numTexLoc;

		//Variables to help control the mouse rotation 
		bool ROTATING;
		float baseX, baseY;
		float lastOffsetX, lastOffsetY;
		float yAngle;
		float xAngle;

		bool isOpen;

		SDL_Window *window;
		SDL_GLContext ogl4context;

	public:
		Viewcontroller();

		bool init();  //initializes SDL, GLEW, and OpenGL
		void display();
		void run();
		bool handleEvents(SDL_Event *theEvent);

		
};

#endif