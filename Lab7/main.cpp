//FileName:		main.cpp
//Programmer:	Dan Cliburn// Chris Bolt, Nicos Sampaolo, Carter Hafif
//Date:			9/9/2014
//Purpose:		This file defines the main() function for Lab 7.

#include "viewcontroller.h"
#include <stdlib.h>
#include <iostream>

int main(int argc, char *argv[])  //main() must take these parameters when using SDL
{
	Viewcontroller vc;
	cout << "This is a laptop. To open or close the laptop press the up arrow key. To rotate the laptop click and drag..." << endl;
	system("pause");
	vc.run();

	system("pause");
	return 0;
}