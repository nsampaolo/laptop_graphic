//The texture mapping code is adapted from Example 6.7 in the OpenGL Programming Guide (8th edition)// Chris Bolt, Nicos Sampaolo, Carter Hafif
#version 400 core

uniform sampler2D tex_image;
uniform int numTextures;

in vec4 color;
in vec2 vs_tex_coord;

out vec4 fColor;

void main()
{
	if (numTextures == 0)
		fColor = color;
	else if(numTextures == 1)
		fColor = texture(tex_image, vs_tex_coord);
}