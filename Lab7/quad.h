//FileName:		quad.h
//Programmer:	Dan Cliburn// Chris Bolt, Nicos Sampaolo, Carter Hafif
//Date:			9/10/2014
//Purpose:		This file defines the header for a quad class. 
//The quad is centered at the origin in the xy plane

#ifndef QUAD_H
#define QUAD_H

#include <glew.h>  //glew.h is supposed to be included before gl.h.  To be safe, you can just include glew.h instead
#include <gl/GLU.h>
#include <string>
using namespace std;

class Quad
{
protected:
	bool initialized;

	GLfloat vertices[4][3];  //each vertex has an x, y, and z value
	GLfloat color[4];
	GLuint VAO;
	GLuint Buffer;  //We'll need one buffer for the vertex positions and one for the texture coordinates

public:
	Quad();

	virtual void defineVertexPositions();
	virtual void defineVerticeColor(const GLfloat *c);
	virtual bool init(const GLfloat *c = 0);  //initializes the model
	virtual void draw();  //renders the model
};

#endif